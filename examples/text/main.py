from pysrgl import Scene
from pysrgl.objects import Text

def main():
	text = Text("VIATA BUNA ESTE", (0.1, 0.1), 0.001, (255, 255, 0))
	text2 = Text("CUMSECADE", (0.1, 0.15), 0.001, (255, 255, 0))
	text3 = Text("CUMSESPUNE", (0.1, 0.2), 0.001, (255, 255, 0))
	viewer = Scene(windowSize = (640, 480), polygons = {"text": text, "text2": text2, "text3": text3})
	viewer.run()

if __name__ == "__main__":
	main()
