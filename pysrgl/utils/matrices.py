"""Basic rotation, tarnslation and scaling matrices."""
import numpy as np
from typing import Union, List, Optional
from scipy.spatial.transform import Rotation as R
from .logger import logger

def translationMatrix(translation: Union[float, List[float]]) -> np.ndarray:
	if isinstance(translation, float):
		translation = [translation, translation, translation]
		logger.debug2("Only one value sent for translation. Translating all 3 axis by same value.")
	dx, dy, dz = translation
	res = np.array([
		[1, 0, 0, 0],
		[0, 1, 0, 0],
		[0, 0, 1, 0],
		[dx, dy, dz, 1]
	], dtype=np.float32)
	return res

def scaleMatrix(scale: Union[float, List[float]]) -> np.ndarray:
	if isinstance(scale, float):
		scale = [scale, scale, scale]
		logger.debug2("Only one value sent for scaling. Scaling all 3 axis by same value.")
	sx, sy, sz = scale
	res = np.array([
		[sx, 0, 0, 0],
		[0, sy, 0, 0],
		[0, 0, sz, 0],
		[0, 0, 0, 1]
	], dtype=np.float32)
	return res

def rotationMatrix(radians: Union[float, List[float]], order: Optional[str]=None):
	if isinstance(radians, float):
		radians = [radians]
	if order is None:
		logger.debug2("Order not set. Setting to XYZ.")
		order = "XYZ"
	res = np.eye(4, dtype=np.float32)
	res[0:3, 0:3] = R.from_euler(order, radians).as_matrix()
	return res
