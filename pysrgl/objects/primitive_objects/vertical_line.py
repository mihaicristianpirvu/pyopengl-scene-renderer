from typing import Tuple
import numpy as np
from .rectangle import rectangle

def verticalLine(startPosition: float, length: float, thickness: float=0.01) -> Tuple[np.ndarray, np.ndarray]:
    topLeft = startPosition
    bottomRight = (startPosition[0] + thickness, startPosition[1] + length)
    return rectangle(topLeft, bottomRight)
