#version 330 core

layout (location = 0) in vec4 vertex; // <vec2 pos, vec2 tex>
out vec2 TexCoords;

void main()
{

    mat4 projection = mat4(
        2, 0, 0, 0, 0, -2, 0, 0, 0, 0, -1, 0, -1, 1, 0, 1
    );

    gl_Position = projection * vec4(vertex.xy, 0.0, 1.0);
    TexCoords = vertex.zw;
}
