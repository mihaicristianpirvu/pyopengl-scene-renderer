#version 330 core

in vec2 TexCoords;
out vec4 FSColor;

uniform sampler2D text;
uniform vec3 textColor;

void main()
{    
    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, TexCoords).r);
    FSColor = vec4(textColor, 1.0) * sampled;
}
